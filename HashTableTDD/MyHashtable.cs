﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTDD
{
    public class KeyPairHash
    {
        public string Key { get; set; }
        public object Value { get; set; }
        public int HashCode { get { return Key.GetHashCode(); } }
    }

    public class MyHashtable
    {
        private LinkedList<KeyPairHash>[] _data { get; set; } = new LinkedList<KeyPairHash>[4];

        public void Add(string key, object value)
        {
            int hashCode = MyHashCodeGenerator.GetHashCode(key);

            if(hashCode >= _data.Length)
            {
                var dataTemp = _data;
                Array.Resize(ref dataTemp, hashCode + 1);
                _data = dataTemp;
            }

            var newKeyPairHash = new KeyPairHash() { Key = key, Value = value };

            if (_data[hashCode] == null)
            {
                _data[hashCode] = new LinkedList<KeyPairHash>();
                _data[hashCode].AddLast(newKeyPairHash);
            }
            else if(_data[hashCode] != null)
            {
                if(_data[hashCode].Any(x => x.Key == key))
                {
                    throw new Exception("The key already exists");
                }
                else
                {
                    _data[hashCode].AddLast(newKeyPairHash);
                }
            }
        }

        public KeyPairHash Get(string key)
        {
            int hashCode = MyHashCodeGenerator.GetHashCode(key);

            if (hashCode >= _data.Length)
                return null;

            return _data[hashCode].Where(x => x.Key == key).FirstOrDefault();
        }

        public void Remove(string key)
        {
            int hashCode = MyHashCodeGenerator.GetHashCode(key);

            if (hashCode >= _data.Length)
                return;

            _data[hashCode].Remove(Get(key));
        }
    }
}
