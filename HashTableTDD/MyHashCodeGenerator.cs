﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTDD
{
    public static class MyHashCodeGenerator
    {
        public static int GetHashCode(string key)
        {
            int hashCode = 0;
            for(int i = 0; i < key.Length; i++)
            {
                hashCode += Convert.ToInt32(key[i]);
                if(hashCode > 100)
                {
                    hashCode = (int)(hashCode * 0.5);
                }
            }
            return hashCode;
        }
    }
}
