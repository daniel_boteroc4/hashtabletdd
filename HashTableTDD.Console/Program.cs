﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTDD.Console
{
    class Program
    {
        static void Main(string[] args)
        {
            LinkedList<Tuple<string,object>>[] _data = new LinkedList<Tuple<string, object>>[4];
            _data[0] = new LinkedList<Tuple<string, object>>();
            _data[1] = new LinkedList<Tuple<string, object>>();
            _data[2] = new LinkedList<Tuple<string, object>>();
            _data[3] = new LinkedList<Tuple<string, object>>();

            Array.Resize(ref _data, _data.Length + 4);

            _data[4] = new LinkedList<Tuple<string, object>>();
            _data[5] = new LinkedList<Tuple<string, object>>();
            _data[6] = new LinkedList<Tuple<string, object>>();
            _data[7] = new LinkedList<Tuple<string, object>>();
        }
    }
}
