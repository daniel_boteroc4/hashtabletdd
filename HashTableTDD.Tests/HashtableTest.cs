﻿using System;
using Microsoft.VisualStudio.TestTools.UnitTesting;

namespace HashTableTDD.Tests
{
    [TestClass]
    public class HashtableTest
    {
        [TestMethod]
        public void AddElementToHashtable_NoException()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            var key = "Key1";
            var element = "Element1";

            //Act
            myHashtable.Add(key, element);

            //Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        public void AddMultipleElementToHashtable_NoException()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            var key = "Key1";
            var element = "Element1";
            var key2 = "Key2";
            var element2 = "Element2";
            var key3 = "Key3";
            var element3 = "Element3";

            //Act
            myHashtable.Add(key, element);
            myHashtable.Add(key2, element2);
            myHashtable.Add(key3, element3);

            //Assert
            Assert.IsTrue(true);
        }

        [TestMethod]
        [ExpectedException(typeof(System.Exception))]
        public void AddSameElementToHashtable_ExpectedException()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            var key = "Key1";
            var element = "Element1";

            //Act
            myHashtable.Add(key, element);
            myHashtable.Add(key, element);

            //Assert
            //No Assert, Exception expected
        }

        [TestMethod]
        public void GetElementFromHashtable()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            var key = "Key1";
            var element = "Element1";

            //Act
            myHashtable.Add(key, element);

            KeyPairHash item = myHashtable.Get(key);

            //Assert
            Assert.IsTrue(item.Key == key);
            Assert.IsTrue(item.Value.ToString() == element);
        }

        [TestMethod]
        public void GetElementFromHashtableThatNotExists()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            string key = "Key1";

            //Act
            KeyPairHash item = myHashtable.Get(key);

            //Assert
            Assert.IsTrue(item == null);
        }

        [TestMethod]
        public void RemoveElement()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            string key = "Key1";
            string value = "Value1";

            //Act
            myHashtable.Add(key, value);
            myHashtable.Remove(key);

            KeyPairHash item = myHashtable.Get(key);

            //Assert
            Assert.IsTrue(item == null);
        }

        [TestMethod]
        public void RemoveElements()
        {
            //Arrange
            var myHashtable = new MyHashtable();
            string key = "Key1";
            string value = "Value1";
            string key2 = "Key2";
            string value2 = "Value2";
            string key3 = "Key3";
            string value3 = "Value3";
            string key4 = "Key4";
            string value4 = "Value4";

            //Act
            myHashtable.Add(key, value);
            myHashtable.Add(key2, value2);
            myHashtable.Add(key3, value3);
            myHashtable.Add(key4, value4);

            myHashtable.Remove(key2);
            myHashtable.Remove(key3);

            KeyPairHash item1 = myHashtable.Get(key);
            KeyPairHash item4 = myHashtable.Get(key4);
            KeyPairHash item2 = myHashtable.Get(key2);
            KeyPairHash item3 = myHashtable.Get(key3);

            //Assert
            Assert.IsTrue(item2 == null);
            Assert.IsTrue(item3 == null);
            Assert.IsTrue(item1 != null && item4 != null);
        }
    }
}
