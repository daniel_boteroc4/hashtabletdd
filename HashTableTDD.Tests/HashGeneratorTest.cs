﻿using Microsoft.VisualStudio.TestTools.UnitTesting;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace HashTableTDD.Tests
{
    [TestClass]
    public class HashGeneratorTest
    {
        [TestMethod]
        public void TestHashCodeGenerator()
        {
            //Arrange
            string Key = "Key1";

            //Act
            int hashCode = MyHashCodeGenerator.GetHashCode(Key);

            //Assert
            Assert.IsTrue(hashCode == 76);
        }
    }
}
